import React from 'react';

class DaftarBuah extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataHargaBuah: [
                { nama: "Semangka", harga: 10000, berat: 1000 },
                { nama: "Anggur", harga: 40000, berat: 500 },
                { nama: "Strawberry", harga: 30000, berat: 400 },
                { nama: "Jeruk", harga: 30000, berat: 1000 },
                { nama: "Mangga", harga: 30000, berat: 500 }
            ],
            inputName: "",
            inputHarga: "",
            inputBerat: "",
            indexcondition: -1,
        }
        this.handleChangeNama = this.handleChangeNama.bind(this);
        this.handleChangeHarga = this.handleChangeHarga.bind(this);
        this.handleChangeBerat = this.handleChangeBerat.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChangeNama(event) {
        this.setState({
            inputName: event.target.value,
        });
    }

    handleChangeHarga(event) {
        this.setState({
            inputHarga: event.target.value,
        });
    }

    handleChangeBerat(event) {
        this.setState({
            inputBerat: event.target.value,
        });
    }

    handleSubmit(event) {
        event.preventDefault()
        if (this.state.indexcondition !== -1) {
            let dataBaru = this.state.dataHargaBuah
            dataBaru[this.state.indexcondition] = {
                nama: this.state.inputName,
                harga: this.state.inputHarga,
                berat: this.state.inputBerat
            }
            this.setState({
                dataHargaBuah: dataBaru,
                inputName: "",
                inputHarga: "",
                inputBerat: "",
                indexcondition: -1,

            })
        } else {
            this.setState({
                dataHargaBuah: [...this.state.dataHargaBuah, {
                    nama: this.state.inputName,
                    harga: this.state.inputHarga,
                    berat: this.state.inputBerat
                }],
                inputName: "",
                inputHarga: "",
                inputBerat: "",
                indexcondition: -1,
            })
        }
    }

    handleEdit(event) {
        let index = event.target.value;
        let data = this.state.dataHargaBuah[index]
        this.setState({
            inputName: data.nama,
            inputHarga: data.harga,
            inputBerat: data.berat,
            indexcondition: index,
        })
    }

    handleDelete(event) {
        let index = event.target.value;
        let data = this.state.dataHargaBuah;
        data.splice(index, 1);
        this.setState({
            dataHargaBuah: data,
        })
    }

    render() {
        return (
            <div>
                <h1>Tabel Harga Buah</h1>
                <table style={{ marginLeft: "auto", marginRight: "auto", border: "1px solid black" }}>
                    <thead style={{ backgroundColor: "#AAAAAA" }}>
                        <tr>
                            <th style={{ columnWidth: "200px" }}>Nama</th>
                            <th style={{ columnWidth: "100px" }}>Harga</th>
                            <th style={{ columnWidth: "100px" }}>Berat</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.dataHargaBuah.map((val, index) => {
                            return (
                                <tr style={{ textAlign: "left", backgroundColor: "white" }}>
                                    <td style={{ backgroundColor: "#FF7F50" }}>{val.nama}</td>
                                    <td style={{ backgroundColor: "#FF7F50" }}>Rp. {val.harga} ,-</td>
                                    <td style={{ backgroundColor: "#FF7F50" }}>{val.berat / 1000} kg</td>
                                    <button onClick={this.handleEdit} value={index}>Edit</button> | <button onClick={this.handleDelete} value={index}>Delete</button>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                <br />
                <form onSubmit={this.handleSubmit}>
                    <label>Nama: </label>
                    <input type="text" value={this.state.inputName} onChange={this.handleChangeNama} />
                    <br />
                    <label>Harga: </label>
                    <input type="text" value={this.state.inputHarga} onChange={this.handleChangeHarga} />
                    <br />
                    <label>Berat : </label>
                    <input type="text" value={this.state.inputBerat} onChange={this.handleChangeBerat} />
                    <br />
                    <button>Submit</button>
                </form>
            </div>
        );
    }
}

export default DaftarBuah;