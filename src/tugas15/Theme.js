import React from "react";
import { ThemenanProvider } from "./ThemeContext";
import Theming from './Theming';

const Theme = () => {
  return (
    <ThemenanProvider>
      <Theming />
    </ThemenanProvider>
  )
}

export default Theme