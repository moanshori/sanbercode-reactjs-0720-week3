import React from "react";
import { BuahProvider, BuahPilihanProvider } from "./BuahContext";
import BuahList from './BuahList';
import BuahForm from './BuahForm';

const Buah = () => {
    return (
        <BuahProvider>
            <BuahPilihanProvider>
                <BuahList />
                <BuahForm />
            </BuahPilihanProvider>
        </BuahProvider>
    )
}

export default Buah