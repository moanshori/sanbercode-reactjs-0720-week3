import React, { useState, createContext } from "react";

export const ThemenanContext = createContext();
export const ThemenanProvider = props => {
    const [dataThemenan, setThemenan] = useState({
        tema: "cyan"
    })

    return (
        <ThemenanContext.Provider value={[dataThemenan, setThemenan]}>
            {props.children}
        </ThemenanContext.Provider>
    )
}