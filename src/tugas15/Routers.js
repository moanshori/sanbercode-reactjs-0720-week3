import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import Tugas11 from '../tugas11/Tugas11';
import Tugas12 from '../tugas12/Tugas12';
import DaftarBuah from '../tugas13/DaftarBuah';
import ManajemenBuah from '../tugas14/ManajemenBuah';
import './Routers.css';
import Buah from '../tugas15/Buah';
import Theme from './Theme';

export default function Routers() {
    return (
        <Router>
            <>
                <nav className="Routers">
                    <ul>
                        <li>
                            <Link to="/">Tugas11</Link>
                        </li>
                        <li>
                            <Link to="/Tugas12">Tugas12</Link>
                        </li>
                        <li>
                            <Link to="/DaftarBuah">Tugas13</Link>
                        </li>
                        <li>
                            <Link to="/ManajemenBuah">Tugas14</Link>
                        </li>
                        <li>
                            <Link to="/Buah">Tugas15</Link>
                        </li>
                        <li style={{ float: "right" }}>
                        <Link to="/Theming">Theme</Link>
                        </li>
                    </ul>
                </nav>
                <Switch>
                    <Route path="/Tugas12">
                        <Tugas12 />
                    </Route>
                    <Route path="/DaftarBuah">
                        <DaftarBuah />
                    </Route>
                    <Route path="/ManajemenBuah">
                        <ManajemenBuah />
                    </Route>
                    <Route path="/Buah">
                        <Buah />
                    </Route>
                    <Route path="/Theming">
                        <Theme />
                    </Route>
                    <Route path="/">
                        <Tugas11 />
                    </Route>
                </Switch>
            </>
        </Router>
    )
}