import React, { useContext, useState } from "react"
import { BuahPilihanContext } from './BuahContext'
import axios from 'axios';

const BuahForm = () => {
    const [dataBuahPilihan, setDataBuahPilihan] = useContext(BuahPilihanContext)

    const handleChangeName = (event) => {
        setDataBuahPilihan({
            id: dataBuahPilihan.id,
            name: event.target.value,
            price: dataBuahPilihan.price,
            weight: dataBuahPilihan.weight
        })
    }

    const handleChangePrice = (event) => {
        setDataBuahPilihan({
            id: dataBuahPilihan.id,
            name: dataBuahPilihan.name,
            price: event.target.value,
            weight: dataBuahPilihan.weight
        })
    }

    const handleChangeWeight = (event) => {
        setDataBuahPilihan({
            id: dataBuahPilihan.id,
            name: dataBuahPilihan.name,
            price: dataBuahPilihan.price,
            weight: event.target.value
        })
    }

    const handleClear = () => {
        setDataBuahPilihan({
            id: "",
            name: "",
            price: "",
            weight: ""
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        let data = {
            name: dataBuahPilihan.name,
            price: dataBuahPilihan.price,
            weight: dataBuahPilihan.weight
        }
        if (dataBuahPilihan.id != "") {
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${dataBuahPilihan.id}`, data)
                .then(res => {
                    console.log("edit")
                    console.log(res.data);
                })
        } else if (dataBuahPilihan.id == "") {
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`, data)
                .then(res => {
                    console.log("submit")
                    console.log(res.data);
                })
        }
        handleClear()
    }

    return (
        <>
            <h1>Form</h1>
            <label>{dataBuahPilihan.id}</label>
            <form onSubmit={handleSubmit}>
                <label>
                    Nama: </label>
                <input type="text" value={dataBuahPilihan.name} onChange={handleChangeName} />
                <br />
                <label>
                    Harga: </label>
                <input type="text" value={dataBuahPilihan.price} onChange={handleChangePrice} />
                <br />
                <label>
                    Berat : </label>
                <input type="text" value={dataBuahPilihan.weight} onChange={handleChangeWeight} />
                <br /> <br />
                <button style={{ padding: "5px 100px", backgroundColor: "#FF401A", color: "white", border: "none" }}>Submit</button>
            </form>
            <button onClick={handleClear} style={{ marginTop: "10px", padding: "5px 100px", backgroundColor: "#FF401A", color: "white", border: "none" }}>Clear</button>
            <br />
        </>
    )

}

export default BuahForm