import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const BuahContext = createContext();
export const BuahProvider = props => {
    const [dataBuah, setDataBuah] = useState([])

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then(res => {
                setDataBuah(res.data.map(item => {
                    return {
                        id: item.id,
                        name: item.name,
                        price: item.price,
                        weight: item.weight
                    }
                }));
            }, [dataBuah])
    })

    return (
        <BuahContext.Provider value={[dataBuah, setDataBuah]}>
            {props.children}
        </BuahContext.Provider>
    )
}

export const BuahPilihanContext = createContext();
export const BuahPilihanProvider = props => {
    const [dataBuahPilihan, setDataBuahPilihan] = useState({
        id: "",
        name: "",
        price: "",
        weight: ""
    })

    return (
        <BuahPilihanContext.Provider value={[dataBuahPilihan, setDataBuahPilihan]}>
            {props.children}
        </BuahPilihanContext.Provider>
    )
}