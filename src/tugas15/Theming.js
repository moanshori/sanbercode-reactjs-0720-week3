import { ThemenanContext } from "./ThemeContext";
import React, { useContext } from "react";

const Theming = () => {
    let [dataTema, setTema] = useContext(ThemenanContext)
    
    const handleCyan = () => {
        setTema({
            tema: "cyan"
        })
    }

    const handleBisque = () => {
        setTema({
            tema: "bisque"
        })
    }

    return (
        <>
            <div style={{background: dataTema.tema, alignItems: "center", margin: "auto", width: "100%", height: "320px" }}>
                <h1>uyee</h1>
                <br />
                <button onClick={handleCyan} >Cyan</button>
                <button onClick={handleBisque}>Bisque</button>
            </div>
        </>
    )
}

export default Theming