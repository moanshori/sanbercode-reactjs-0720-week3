import React, { useState, useContext } from "react";
import { BuahContext, BuahPilihanContext } from './BuahContext';
import axios from 'axios';

const BuahList = () => {
    const [dataBuah] = useContext(BuahContext)
    const [dataBuahPilihan, setDataBuahPilihan] = useContext(BuahPilihanContext)

    const handleEdit = (event) => {
        console.log("edit " + event.target.value)
        let data = dataBuah.find(x =>
            x.id == event.target.value
        )
        setDataBuahPilihan({
            id: event.target.value,
            name: data.name,
            price: data.price,
            weight: data.weight
        })
        console.log(dataBuahPilihan)
    }

    const handleDelete = (event) => {
        console.log("delete " + event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${event.target.value}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    }

    return (
        <>
            <h1>Tabel Harga Buah</h1>
            <table style={{ marginLeft: "auto", marginRight: "auto", border: "1px solid black", color: "white" }}>
                <thead style={{ backgroundColor: "#AAAAAA" }}>
                    <tr>
                        <th style={{ columnWidth: "50px" }}>Id</th>
                        <th style={{ columnWidth: "150px" }}>Nama</th>
                        <th style={{ columnWidth: "100px" }}>Harga</th>
                        <th style={{ columnWidth: "100px" }}>Berat</th>
                    </tr>
                </thead>
                <tbody>
                    {dataBuah.map((val, index) => {
                        return (
                            <tr style={{ textAlign: "left", backgroundColor: "white" }}>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.id}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.name}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>Rp. {val.price} ,-</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.weight / 1000} kg</td>
                                <button onClick={handleEdit} value={val.id}>Edit</button>|<button onClick={handleDelete} value={val.id}>Delete</button>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default BuahList