import React from 'react';

class Countdown extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            time: 0,
        }

    }

    render() {
        return (
            <>
                {
                this.state.time >= 0 &&
                    <h3 style={{ textAlign: "right", float: "right" }}>
                        hitung mundur : {this.state.time}
                    </h3>
                }
            </>
        )
    }

    componentDidUpdate() {
        // console.log(this.state.time)
        if (this.state.time < 0) {
            this.componentWillUnmount();
        }
    }

    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start })
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    tick() {
        this.setState({
            time: this.state.time - 1
        });
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

export default Countdown;