import React from 'react';

class CurrentTime extends React.Component {
    constructor(props) {
        super(props)
        var d = new Date().toLocaleTimeString();
        this.state = {
            ctime: d,
            time: 0
        }
    }

    render() {
        return (
            <>{
                this.state.time >= 0 &&
                <h3 style={{ textAlign: "left", float: "left" }}>
                    Sekarang jam : {this.state.ctime}
                </h3>
    }
            </>
        )
    }

    componentDidUpdate() {
        // console.log(this.state.time)
        if (this.state.time < 0) {
            this.componentWillUnmount();
        }
    }

    componentDidMount() {
        if (this.props.start !== undefined) {
            this.setState({ time: this.props.start })
        }
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    tick() {
        this.setState({
            ctime: new Date().toLocaleTimeString(),
            time: this.state.time - 1
        });
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }
}

export default CurrentTime;