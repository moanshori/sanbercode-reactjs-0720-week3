import React from 'react';
import CountDown from './Countdown';
import CurrentTime from './CurrentTime';

class Tugas12 extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <>
                <CurrentTime start={101} />
                <CountDown start={101} />
            </>
        )
    }
}

export default Tugas12