import React from 'react';
// import logo from './logo.svg';
import './App.css';
// import Tugas11 from './tugas11/Tugas11';
// import Countdown from './tugas12/Countdown';
// import CurrentTime from './tugas12/CurrentTime';
// import Tugas12 from './tugas12/Tugas12';
// import DaftarBuah from './tugas13/DaftarBuah';
// import ManajemenBuah from './tugas14/ManajemenBuah';
// import Movie from './tugas15/Movie';
import Routers from './tugas15/Routers';

function App() {
  return (
    <div className="App">
      {/* Tugas 11 */}
      {/* <Tugas11 /> */}

      {/* Tugas 12 */}
      {/* <CurrentTime start={101}/>
      <Countdown start={101}/> */}
      {/* <Tugas12 /> */}

      {/* Tugas 13 */}
      {/* <DaftarBuah /> */}

      {/* Tugas 14 */}
      {/* <ManajemenBuah /> */}
      {/* <Example /> */}

      {/* Tugas 15 */}
      {/* <Movie /> */}
      <h1>Learning ReactJS</h1>
      <Routers />
    </div>
  );
}

export default App;
