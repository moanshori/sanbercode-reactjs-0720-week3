import React, { useState, useEffect } from 'react';
import axios from 'axios';

function ManajemenBuah() {
    const [dataBuah, setDataBuah] = useState([])
    const [inputName, setinputName] = useState("")
    const [inputPrice, setinputPrice] = useState("")
    const [inputWeight, setinputWeight] = useState("")
    const [editCond, seteditCond] = useState(-1)
    const [isEdit, setisEdit] = useState("")

    useEffect(() => {
        axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
            .then(res => {
                setDataBuah(res.data.map(item => {
                    return {
                        id: item.id,
                        name: item.name,
                        price: item.price,
                        weight: item.weight
                    }
                }));
            }, [dataBuah])
    })

    const handleChangeName = (event) => {
        setinputName(event.target.value);
    }

    const handleChangePrice = (event) => {
        setinputPrice(event.target.value);
    }

    const handleChangeWeight = (event) => {
        setinputWeight(event.target.value);
    }

    const handleEdit = (event) => {
        console.log("edit " + event.target.value)
        let data = dataBuah.find(x =>
            x.id == event.target.value
        )
        console.log(data)
        setinputName(data.name)
        setinputPrice(data.price)
        setinputWeight(data.weight)
        seteditCond(data.id)
        setisEdit("Edit id: " + data.id)
    }

    const handleDelete = (event) => {
        console.log("delete " + event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${event.target.value}`)
            .then(res => {
                console.log(res);
                console.log(res.data);
            })
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        let data = {
            name: inputName,
            price: inputPrice,
            weight: inputWeight
        }
        if (editCond != -1) {
            axios.put(`http://backendexample.sanbercloud.com/api/fruits/${editCond}`, data)
                .then(res => {
                    console.log("edit")
                    console.log(res.data);
                })
        } else {
            axios.post(`http://backendexample.sanbercloud.com/api/fruits`, data)
                .then(res => {
                    console.log("submit")
                    console.log(res.data);
                })
        }
        handleClear()
    }

    const handleClear = () => {
        setinputName("");
        setinputPrice("");
        setinputWeight("");
        seteditCond(-1)
        setisEdit("")
    }

    return (
        <>
            <h1>Tabel Harga Buah</h1>
            <table style={{ marginLeft: "auto", marginRight: "auto", border: "1px solid black", color: "white" }}>
                <thead style={{ backgroundColor: "#AAAAAA" }}>
                    <tr>
                        <th style={{ columnWidth: "50px" }}>Id</th>
                        <th style={{ columnWidth: "150px" }}>Nama</th>
                        <th style={{ columnWidth: "100px" }}>Harga</th>
                        <th style={{ columnWidth: "100px" }}>Berat</th>
                    </tr>
                </thead>
                <tbody>
                    {dataBuah.map((val, index) => {
                        return (
                            <tr style={{ textAlign: "left", backgroundColor: "white" }}>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.id}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.name}</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>Rp. {val.price} ,-</td>
                                <td style={{ backgroundColor: "#FF7F50" }}>{val.weight / 1000} kg</td>
                                <button onClick={handleEdit} value={val.id}>Edit</button>|<button onClick={handleDelete} value={val.id}>Delete</button>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <h1>Form</h1>
            <label>{isEdit}</label>
            <form onSubmit={handleSubmit}>
                <label>
                    Nama: </label>
                <input type="text" value={inputName} onChange={handleChangeName} />
                <br />
                <label>
                    Harga: </label>
                <input type="text" value={inputPrice} onChange={handleChangePrice} />
                <br />
                <label>
                    Berat : </label>
                <input type="text" value={inputWeight} onChange={handleChangeWeight} />
                <br /> <br />
                <button style={{ padding: "5px 100px", backgroundColor: "#FF401A", color: "white", border: "none" }}>Submit</button>
            </form>
            <button onClick={handleClear} style={{ marginTop: "10px", padding: "5px 100px", backgroundColor: "#FF401A", color: "white", border: "none" }}>Clear</button>
            <br />
        </>
    );
}

export default ManajemenBuah;