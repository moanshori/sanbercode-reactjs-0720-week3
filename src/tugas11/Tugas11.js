import React from 'react';
import './Tugas11.css';

let dataHargaBuah = [
    { nama: "Semangka", harga: 10000, berat: 1000 },
    { nama: "Anggur", harga: 40000, berat: 500 },
    { nama: "Strawberry", harga: 30000, berat: 400 },
    { nama: "Jeruk", harga: 30000, berat: 1000 },
    { nama: "Mangga", harga: 30000, berat: 500 }
]

class ShowName extends React.Component {
    render() {
        return <p>{this.props.name}</p>
    }
}

class ShowPrice extends React.Component {
    render() {
        return <p>{this.props.price}</p>
    }
}

class ShowWeight extends React.Component {
    render() {
        return <p>{this.props.weight / 1000} kg</p>
    }
}

class Tugas11 extends React.Component {
    render() {
        return (
            <div className="content">
                <h1>Tabel Harga Buah</h1>
                <table>
                    <thead>
                        <tr>
                            <th style={{width: "20%"}}>Nama</th>
                            <th style={{width: "10%"}}>Harga</th>
                            <th style={{width: "10%"}}>Berat</th>
                        </tr>
                    </thead>
                    <tbody>
                        {dataHargaBuah.map(item => {
                            return (
                                <tr>
                                    <td><ShowName name={item.nama} /></td>
                                    <td><ShowPrice price={item.harga} /></td>
                                    <td><ShowWeight weight={item.berat} /></td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Tugas11;